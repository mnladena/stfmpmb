<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_center">
                <h3>Pembayaran</h3>
              </div>

              <div class="left">
                <div class="">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="pembayaran.php">Pembayaran</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Bayar Pendaftaran</li>
                    </ol>
                </div>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="form_box">

                  <?php for ($i = 0; $i < 6; $i++){ 
                        echo '
                    <div class="col-md-4 col-sm-4 col-xs-12 list-berkas">
                      <div class="form-group border">
                        <div class="title-berkas control-label col-md-12 col-sm-12 col-xs-12">Ijazah
                        </div>
                        <div class="input-file col-md-12 col-sm-12 col-xs-12">
                          <div class="img-wrap">
                            <img data-toggle="modal" data-target=".preview-img" id="preview-img'.$i.'" src="images/ijazah.jpg" />
                          </div>
                          <div class="clearfix"></div>  
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>';}?>

                  <div class="clearfix"></div>
                  <div class="ln_solid"></div>

                  <div class="center">
                    <a href="pembayaran.php" class="btn btn-primary">Kembali</a>
                  </div>

            </div>

            <!-- modal -->
                  <div class="modal fade preview-img" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">File Berkas</h4>
                        </div>
                        <div class="modal-body">
                          <img class="preview-gbr" src="">
                        </div>
                        <div class="modal-footer center ">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>

                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<script type="text/javascript">
  $('.img-wrap img').on("click",function(){
          var Imgs=$(this).attr('src');
          $('.preview-gbr').attr('src',Imgs);
        });
</script>
