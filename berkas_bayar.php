<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_center">
                <h3>Pembayaran</h3>
              </div>

              <div class="left">
                <div class="">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="pembayaran.php">Pembayaran</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Bayar Pendaftaran</li>
                    </ol>
                </div>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="form_box-bayar">

                  <h3>Detail Pembayaran</h3>

                  <div class="nama-mahasiswa">Rini Handayani - 0345431</div>

                  <table class="form-preview table table-striped">
                    <tr>
                      <td>Biaya Pendaftaran</td>
                      <td>Rp 500.000</td>
                    </tr>
                    <tr>
                      <td>Id Unik</td>
                      <td>5BU35</td>
                    </tr>
                    <tr>
                      <td>Nilai yang harus ditransfer</td>
                      <td>Rp 500.056</td>
                    </tr>
                    <tr>
                      <td>Bank Tujuan</td>
                      <td>BCA</td>
                    </tr>
                    <tr>
                      <td>Bukti Bayar</td>
                      <td><img src="images/struk.jpg"></td>
                    </tr>
                  </table>

                  <div class="ln_solid"></div>

                  <div class="center">
                    <a href="pembayaran.php" class="btn btn-primary">Kembali</a>
                  </div>

            </div>

          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>
