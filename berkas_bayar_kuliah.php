<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_center">
                <h3>Pembayaran</h3>
              </div>

              <div class="left">
                <div class="">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="pembayaran.php">Pembayaran</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Bayar Pendaftaran</li>
                    </ol>
                </div>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="form_box-bayar">

                  <h3>Detail Pembayaran</h3>

                  <div class="nama-mahasiswa">Rini Handayani - 0345431</div>

                  <table class="form-preview table table-striped">
                    <tr>
                      <td>Biaya Administrasi dll</td>
                      <td>Rp 1.100.000</td>
                    </tr>
                    <tr>
                      <td>BOP</td>
                      <td>Rp 7.100.000</td>
                    </tr>
                    <tr>
                      <td>BPP (Full)</td>
                      <td>Rp 1.100.000</td>
                    </tr>

                    <tr class="borderline">
                      <td>Total biaya</td>
                      <td><b>Rp 13.100.000</b></td>
                    </tr>
                    <tr>
                      <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                      <td>Id Unik</td>
                      <td>#U35</td>
                    </tr>
                    <tr>
                      <td>Nilai yang harus ditransfer</td>
                      <td><b>Rp 13.500.056</b></td>
                    </tr>
                    <tr>
                      <td>Bank Tujuan</td>
                      <td>BCA</td>
                    </tr>
                    <tr>
                      <td>Bukti Bayar</td>
                      <td><img src="images/struk.jpg"></td>
                    </tr>
                  </table>

                  <div class="ln_solid"></div>

                  <div class="center">
                    <a href="form_bayar_kuliah-lanjut.php" class="btn btn-success">Kirim Bukti Bayar</a>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".submit-form">Bukti Bayar</button>
                    <a href="pembayaran.php" class="btn btn-default">Kembali</a>
                  </div>

                  <!-- modal -->
                  <div class="modal fade submit-form" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-md">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">Konfirmasi Submit Pembayaran</h4>
                        </div>
                        <div class="modal-body">
                          <h3>Pembayaran</h3>
                          <table class="form-preview table table-striped">
                            <tr>
                              <td>Biaya Administrasi dll</td>
                              <td>Rp 1.100.000</td>
                            </tr>
                            <tr>
                              <td>BOP</td>
                              <td>Rp 7.100.000</td>
                            </tr>
                            <tr>
                              <td>BPP (Full)</td>
                              <td>Rp 1.100.000</td>
                            </tr>

                            <tr class="borderline">
                              <td>Total biaya</td>
                              <td><b>Rp 13.100.000</b></td>
                            </tr>
                            <tr>
                              <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                              <td>Id Unik</td>
                              <td>#U35</td>
                            </tr>
                            <tr>
                              <td>Nilai yang harus ditransfer</td>
                              <td><b>Rp 13.500.056</b></td>
                            </tr>
                            <tr>
                              <td>Bank Tujuan</td>
                              <td>BCA</td>
                            </tr>
                            <tr>
                              <td>Bukti Bayar</td>
                              <td><img src="images/struk.jpg"></td>
                            </tr>
                          </table>

                          <!-- message -->
                          <div class="alert alert-warning" role="alert">
                            <strong>Catatan:</strong> Data yang disubmit akan diverifikasi dahulu dan tidak bisa diubah
                          </div>

                        </div>

                        <div class="modal-footer center ">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                        </div>

                </div>
              </div>
            </div>

            </div>

          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>
