<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_center">
                <h3>Formulir Pendaftaran</h3>
              </div>

              <div class="left">
                <div class="">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="pendaftaran.php">Formulir Pendaftaran</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Biodata</li>
                    </ol>
                </div>
              </div>

          </div>

          <div class="clearfix"></div>

          <div class="mt15">

            <form id="" class="form-horizontal form-label-left">

                <h3>Biodata</h3>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jenis Pendaftaran
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select disabled id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="">Regular</option>
                          <option value="">Non-Regular</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jenjang
                      </label>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="radio pilihan-jawaban">
                              <input disabled type="radio" class="flat left" value="jenjang1" id="jenjang1" name="jenjang"> <span class="inp-text left">D3 Farmasi</span>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="radio pilihan-jawaban">
                          <input disabled type="radio" class="flat left" checked value="jenjang2" id="jenjang2" name="jenjang"> <span class="inp-text left">S1 Farmasi</span>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input disabled type="text" id="" class="form-control col-md-7 col-xs-12" value="Rini Handayani">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jenis Kelamin
                      </label>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="radio pilihan-jawaban">
                              <input disabled type="radio" class="flat left" value="jenis1" id="jenis1" name="jenis"> <span class="inp-text left">Laki-laki</span>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="radio pilihan-jawaban">
                          <input disabled type="radio" class="flat left" checked value="jenis2" id="jenis2" name="jenis"> <span class="inp-text left">Perempuan</span>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Tempat/Tanggal Lahir
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input disabled type="text" id="" class="form-control col-md-7 col-xs-12" value="Banjarmasin">
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class='input-group date'>
                            <input disabled type='text' class="form-control"  id='datetimepicker7' value="22/12/2005"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Agama
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select disabled id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="">Islam</option>
                          <option value="">Kristen</option>
                          <option value="">Buddha</option>
                          <option value="">Hindu</option>
                          <option value="">Kepercayaan</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Alamat KTP
                      </label>
                      <div class="col-md-5 col-sm-5 col-xs-12">
                        <input disabled type="text" id="" class="form-control col-md-7 col-xs-12" value="Jl. Jalak Harupat 20">
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3">
                          <select disabled id="" class="form-control">
                            <option value="">Pilih Kota</option>
                            <option selected value="">Kota Bogor</option>
                            <option value="">Kabupaten Bogor</option>
                            <option value="">Jakarta</option>
                            <option value="">Tangerang</option>
                            <option value="">Bekasi</option>
                          </select>
                        </div>
                    </div>
                        
                    <div class="form-group">
                      <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3">
                          <select disabled id="" class="form-control">
                            <option value="">Pilih Kecamatan</option>
                            <option selected value="">Kecamatan Bogor Timur</option>
                            <option value="">Kabupaten Bogor</option>
                            <option value="">Jakarta</option>
                            <option value="">Tangerang</option>
                            <option value="">Bekasi</option>
                          </select>
                        </div>
                      </div>

                    <div class="form-group">
                      <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3">
                          <select disabled id="" class="form-control">
                            <option value="">Pilih Kelurahan</option>
                            <option selected value="">Kelurahan Loji</option>
                            <option value="">Kabupaten Bogor</option>
                            <option value="">Jakarta</option>
                            <option value="">Tangerang</option>
                            <option value="">Bekasi</option>
                          </select>
                        </div>
                      </div>

                    <div class="form-group">
                      <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3">
                          <input disabled type="text" id="" placeholder="Kode Pos" class="form-control col-md-7 col-xs-12" value="16551">
                        </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Alamat Tempat Tinggal
                      </label>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="radio pilihan-jawaban">
                              <input disabled type="radio" class="flat left" selected value="alamat1" id="alamat1" name="alamat"> <span class="inp-text left">Sesuai KTP</span>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="radio pilihan-jawaban">
                          <input disabled type="radio" class="flat left" value="alamat2" id="alamat2" name="alamat"> <span class="inp-text left">Berbeda dari alamat KTP</span>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Kewarganegaraan
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select disabled id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="">WNI</option>
                          <option value="">WNA</option>
                        </select>
                      </div>

                      <!-- kalau WNA muncul input text ini -->
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input disabled type="text" id="" placeholder="Isi Negara Asal" class="form-control col-md-7 col-xs-12" value="">
                      </div>

                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Sekolah Asal
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select disabled id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="">SMA</option>
                          <option value="">SMK</option>
                        </select>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input disabled type="text" id="" placeholder="Nama Sekolah" class="form-control col-md-7 col-xs-12" value="SMA 22 Bogor">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Status Pekerjaan
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select disabled id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="">Siswa</option>
                          <option value="">Karyawan</option>
                          <option value="">Lainnya</option>
                        </select>
                      </div>

                      <!-- kalau Lainnya muncul input text ini -->
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input disabled type="text" id="" placeholder="Isi Jenis Pekerjaan" class="form-control col-md-7 col-xs-12" value="">
                      </div>

                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Golongan Darah
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select disabled id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="">AB</option>
                          <option value="">B</option>
                          <option value="">O</option>
                          <option value="">AB</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nomor HP
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input disabled type="text" id="" class="form-control col-md-7 col-xs-12" value="0812234567">
                      </div>
                    </div>

                <div class="ln_solid"></div>

                <h3>Orang Tua/Wali</h3>

                    <div class="form-group">
                        <h4 class="control-label col-md-3 col-sm-3 col-xs-12">Ayah
                        </h4>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input disabled type="text" id="" class="form-control col-md-7 col-xs-12" value="Ponirin">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Pekerjaan
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select disabled id="" class="form-control">
                          <option value="">Choose..</option>
                          <option value="">Wiraswasta</option>
                          <option selected value="">Petani</option>
                          <option value="">Karyawan</option>
                          <option value="">Driver</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nomor HP
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input disabled type="text" id="" class="form-control col-md-7 col-xs-12" value="0811123345">
                      </div>
                    </div>

                    <div class="form-group">
                        <h4 class="control-label col-md-3 col-sm-3 col-xs-12">Ibu
                        </h4>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input disabled type="text" id="" class="form-control col-md-7 col-xs-12" value="Taniem">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Pekerjaan
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select disabled id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="">Ibu Rumah Tangga</option>
                          <option value="">Petani</option>
                          <option value="">Karyawan</option>
                          <option value="">Driver</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nomor HP
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input disabled type="text" id="" class="form-control col-md-7 col-xs-12" value="083221112">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Penghasilan
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select disabled id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value=""> <= 1 Juta</option>
                          <option value=""> 1 Juta - 5 Juta</option>
                          <option value="">5 Juta - 10 Juta</option>
                          <option value=""> > 10 Juta </option>
                        </select>
                      </div>
                    </div>

                    <div class="ln_solid"></div>

                    <h3>Referensi</h3>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Informasi STF dari
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select disabled id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="">Media Online</option>
                          <option value="">Keluarga</option>
                          <option value="">Teman</option>
                          <option value="">Iklan</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Referal
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input disabled type="text" id="" class="form-control col-md-7 col-xs-12" value="detik.com">
                      </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <a href="pendaftaran.php" class="btn btn-primary">Kembali</a>
                      </div>
                    </div>

                  </form>

                
          </div>
        </div>
      </div>
      <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>
