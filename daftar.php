<?php include "include/head.php" ?>

  <script type="text/javascript">
      var onloadCallback = function() {
        grecaptcha.render('html_element', {
          'sitekey' : 'your_site_key'
        });
      };
    </script>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <!-- <div class="left-main">
          <div class="left-main-content">
            <div class="big-title-home">Proses Kegiatan Penerimaan Mahasiswa Baru</div>
            
          </div>
      </div> -->

      <div class="login_wrapper">
        <div class="form login_form">
          <div class="site_logo center"><img src="images/logo.png" alt=""></div>
          <div class="center">Daftarkan diri Anda sekarang</div>
          <section class="login_content">
            <form class="form-daftar">
              <h1>Daftar</h1>

              <div class="form-group">
                <label class="control-label" for="">Jenis Pendaftaran
                </label>
                <div class="">
                  <select required="required" id="" class="form-control">
                    <option value="">Choose..</option>
                    <option selected value="1">Regular</option>
                    <option value="2">Non-Regular</option>
                  </select>
                </div>
              </div>
               
              <div class="form-group">
                <label class="control-label" for="">Jenjang
                </label>
                <div class="">
                  <div class="radio pilihan-jawaban">
                        <input required="required" type="radio" class="flat left" checked value="jenjang1" id="jenjang1" name="jenjang"> <span class="inp-text left">D3 Farmasi</span>
                  </div>
                </div>
                <div class="">
                  <div class="radio pilihan-jawaban">
                    <input required="required" type="radio" class="flat left" value="jenjang2" id="jenjang2" name="jenjang"> <span class="inp-text left">S1 Farmasi</span>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>

              <div class="form-group">
                <label class="control-label" for="">Nama
                </label>
                <div class="">
                  <input required="required" type="text" id="" class="form-control col-md-7 col-xs-12" value="Mahathir Mohammad">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label" for="">Jenis Kelamin
                </label>
                <div class="">
                  <div class="radio pilihan-jawaban">
                        <input type="radio" class="flat left" checked value="jenis1" id="jenis1" name="jenis"> <span class="inp-text left">Laki-laki</span>
                  </div>
                </div>
                <div class="">
                  <div class="radio pilihan-jawaban">
                    <input type="radio" class="flat left" value="jenis2" id="jenis2" name="jenis"> <span class="inp-text left">Perempuan</span>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>

              <div class="form-group">
                 <label class="control-label" for="">Email
                </label>
                <input type="text" class="form-control" placeholder="Email" required="" />
              </div>

              <div class="form-group">
                 <label class="control-label" for="">No.HP
                </label>
                <input type="text" class="form-control" placeholder="No. HP" required="" />
              </div>

              <div class="form-group">
                <label for="">Pas Foto</label>
                <input type="file" class="form-control" required="" />
              </div>

              <div id="html_element"></div>

              <div class="form-group">
                <button class="btn btn-success submit" href="index.php">Daftar</button>
                <button class="btn btn-default submit" href="Login.php">Batal</button>
              </div>

              <div class="clearfix"></div>

            </form>
            <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer>
            </script>
          </section>
        </div>

      </div>
    </div>

    <div class="log_footer">
    <?php include "include/footer.php" ?>
    </div>
