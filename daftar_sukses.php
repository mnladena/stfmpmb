<?php include "include/head.php" ?>

  <script type="text/javascript">
      var onloadCallback = function() {
        grecaptcha.render('html_element', {
          'sitekey' : 'your_site_key'
        });
      };
    </script>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="left-main">
          <div class="left-main-content">
            <div class="big-title-home">Proses Kegiatan Penerimaan Mahasiswa Baru</div>
            
          </div>
      </div>

      <div class="login_wrapper side_log reg-success">
        <div class="form login_form">
          <section class="login_content">
            <div class="icon-log .log-sm">
              <span class="fa fa-check"></span>
            </div>
            <div class="center big_title">Registrasi Berhasil!</div>
            <div class="center med_title">Silahkan cek email Anda</div>
            <!-- <div class="icon-log">
              <span class="fa fa-envelope"></span>
            </div> -->
            <a class="btn btn-primary" href="login.php">Kembali ke halaman Login</a>
          </section>
        </div>

      </div>
    </div>
    <?php include "include/footer.php" ?>
