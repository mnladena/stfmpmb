<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_center">
                <h3>Pembayaran</h3>
              </div>

              <div class="left">
                <div class="">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="pembayaran.php">Pembayaran</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Bayar Pendaftaran</li>
                    </ol>
                </div>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="form_box-bayar">

                <form action="#" class="form-horizontal form-label-left">

                  <h3>Detail Pembayaran</h3>

                  <div class="nama-mahasiswa">Rini Handayani - 0345431</div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Biaya Administrasi dll
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">Rp 1.100.000</span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">BOP (Biaya Operasional Perkuliahan)
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">Rp 14.100.000</span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Cara Pembayaran
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <select required="required" id="" class="form-control">
                        <option value="">Pilih Cara Pembayaran</option>
                        <option value="1">Bayar Full</option>
                        <option value="2">Cicilan 2x bayar</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Biaya yang akan dibayar
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">Rp 7.100.000</span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Sisa pembayaran
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">Rp 7.100.000</span>
                    </div>
                  </div>


                   <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">BPP (Bangunan)
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">Rp 14.100.000</span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Cara Pembayaran
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <select required="required" id="" class="form-control">
                        <option value="">Pilih Cara Pembayaran</option>
                        <option value="1">Bayar Full</option>
                        <option value="2">Cicilan 2x bayar</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Biaya yang akan dibayar
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">Rp 7.100.000</span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Sisa pembayaran
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">Rp 0</span>
                    </div>
                  </div>


                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Total biaya
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">Rp 18.100.000</span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Id Unik
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">#VCB</span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nilai yang harus ditransfer
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text jumbo-text">Rp 500.056</span>
                      <div class="info alert alert-info">Harap mentransfer sesuai dengan nilai yang tertulis agar tidak menghambat proses verifikasi</div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Pilihan Bank Transfer
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <select required="required" id="" class="form-control">
                        <option value="">Pilih Bank</option>
                        <option value="1">BCA</option>
                        <option value="2">BNI</option>
                        <option value="3">Mandiri</option>
                        <option value="4">BRI</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Bukti bayar
                    </label>
                    <div class="input-file col-md-6 col-sm-6 col-xs-12">
                      <input type="file" name="" id="img1"><label>Browse</label>
                      <img data-toggle="modal" data-target=".preview-img" id="preview-img1" />
                    </div>
                  </div>
                  
                  <div class="form-group">
                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">
                      Rekening tujuan
                    </label>
                    <div class="clearfix"></div>

                    <div class="daftar-bank col-md-5 col-sm-5 col-xs-12">
                      <img class="logo-bank" src="images/bni.png">
                      <div>Bank BNI, Tigaraksa</div>
                      <div>No. Rek: 9098898773</div>
                    </div>
                    <div class="daftar-bank col-md-5 col-sm-5 col-xs-12">
                      <img class="logo-bank"  src="images/bca.png">
                      <div>Bank BCA, Tigaraksa</div>
                      <div>No. Rek: 9098898773</div>
                    </div>
                    <div class="daftar-bank col-md-5 col-sm-5 col-xs-12">
                      <img class="logo-bank"  src="images/mandiri.png">
                      <div>Bank Mandiri, Tigaraksa</div>
                      <div>No. Rek: 9098898773</div>
                    </div>
                    <div class="daftar-bank col-md-5 col-sm-5 col-xs-12">
                      <img class="logo-bank"  src="images/bri.png">
                      <div>Bank BRI, Tigaraksa</div>
                      <div>No. Rek: 9098898773</div>
                    </div>
                  </div>

                  <!-- modal -->
                  <div class="modal fade preview-img" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">File Berkas</h4>
                        </div>
                        <div class="modal-body">
                          <img class="preview-gbr" src="">
                        </div>
                        <div class="modal-footer center ">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>

                </div>
              </div>
            </div>

                  <!-- message -->
                  <div class="alert alert-warning" role="alert">
                    <strong>Catatan:</strong> Proses verifikasi memerlukan waktu 1x24 jam
                  </div>

                  <div class="alert alert-" role="alert">
                    <strong>Status:</strong> <span class="text-danger"> Belum Mengirim</span>
                  </div>

                  <div class="ln_solid"></div>

                  <div>
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                      <button type="button" class="btn btn-success" data-toggle="modal" data-target=".submit-form">Simpan</button>
                      <button type="button" class="btn btn-default">Batal</button>
                    </div>
                  </div>

                  <!-- modal -->
                  <div class="modal fade submit-form" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-md">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">Konfirmasi Submit Pembayaran</h4>
                        </div>
                        <div class="modal-body">
                          <h3>Pembayaran</h3>
                          <table class="form-preview table table-striped">
                            <tr>
                              <td>Biaya Administrasi dll</td>
                              <td>Rp 1.100.000</td>
                            </tr>
                            <tr>
                              <td>BOP</td>
                              <td>Rp 7.100.000</td>
                            </tr>
                            <tr>
                              <td>BPP (Full)</td>
                              <td>Rp 1.100.000</td>
                            </tr>

                            <tr class="borderline">
                              <td>Total biaya</td>
                              <td><b>Rp 13.100.000</b></td>
                            </tr>
                            <tr>
                              <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                              <td>Id Unik</td>
                              <td>#U35</td>
                            </tr>
                            <tr>
                              <td>Nilai yang harus ditransfer</td>
                              <td><b>Rp 13.500.056</b></td>
                            </tr>
                            <tr>
                              <td>Bank Tujuan</td>
                              <td>BCA</td>
                            </tr>
                            <tr>
                              <td>Bukti Bayar</td>
                              <td><img src="images/struk.jpg"></td>
                            </tr>
                          </table>

                          <!-- message -->
                          <div class="alert alert-warning" role="alert">
                            <strong>Catatan:</strong> Data yang disubmit akan diverifikasi dahulu dan tidak bisa diubah
                          </div>

                        </div>

                        <div class="modal-footer center ">
                          <button type="submit" class="btn btn-primary">Konfirmasi</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        </div>

                </div>
              </div>
            </div>

                </form>

            </div>

          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>
<script type="text/javascript">
  function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                imgId = '#preview-'+$(input).attr('id');
                $(imgId).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
          var imgIds = '#preview-'+$(input).attr('id');
          $(imgIds).on("click",function(){
            var Imgs=$(this).attr('src');
            $('.preview-gbr').attr('src',Imgs);
          });
      }


      $(".input-file input[type='file']").change(function(){
        readURL(this);
      });
</script>