<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_center">
                <h3>Pembayaran</h3>
              </div>

              <div class="left">
                <div class="">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="pembayaran.php">Pembayaran</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Bayar Pendaftaran</li>
                    </ol>
                </div>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="form_box-bayar">

                <form action="#" class="form-horizontal form-label-left">

                  <h3>Detail Pembayaran</h3>

                  <div class="nama-mahasiswa">Rini Handayani - 0345431</div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Biaya Administrasi dll
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">Rp 1.100.000</span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">BOP (Biaya Operasional Perkuliahan)
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">Rp 14.100.000</span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Cara Pembayaran
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <select required="required" id="" class="form-control">
                        <option value="">Pilih Cara Pembayaran</option>
                        <option value="1">Bayar Full</option>
                        <option value="2">Cicilan 2x bayar</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Biaya yang akan dibayar
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">Rp 7.100.000</span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Sisa pembayaran
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">Rp 7.100.000</span>
                    </div>
                  </div>


                   <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">BPP (Bangunan)
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">Rp 14.100.000</span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Cara Pembayaran
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <select required="required" id="" class="form-control">
                        <option value="">Pilih Cara Pembayaran</option>
                        <option value="1">Bayar Full</option>
                        <option value="2">Cicilan 2x bayar</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Biaya yang akan dibayar
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">Rp 7.100.000</span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Sisa pembayaran
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">Rp 0</span>
                    </div>
                  </div>


                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Total biaya
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">Rp 18.100.000</span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Id Unik
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text">#VCB</span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nilai yang harus ditransfer
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <span class="inp-text jumbo-text">Rp 500.056</span>
                      <div class="info alert alert-info">Harap mentransfer sesuai dengan nilai yang tertulis agar tidak menghambat proses verifikasi</div>
                    </div>
                  </div>

                  <div class="ln_solid"></div>

                  <div>
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                      <button type="button" class="btn btn-success" data-toggle="modal" data-target=".submit-form">Simpan</button>
                      <button type="button" class="btn btn-default">Batal</button>
                    </div>
                  </div>

                  <!-- modal -->
                  <div class="modal fade submit-form" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-md">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">Konfirmasi Submit Pembayaran</h4>
                        </div>
                        <div class="modal-body center">
                          
                          Apakah anda yakin ingin menyimpan cara pembayaran?
                          <br>
                          Cara pembayaran yang telah disimpan tidak dapan diubah

                        </div>

                        <div class="modal-footer center ">
                          <a href="form_bayar_kuliah-lanjut.php" class="btn btn-primary">Konfirmasi</a>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        </div>

                </div>
              </div>
            </div>

                </form>

            </div>

          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>
