<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_center">
                <h3>Pembayaran</h3>
              </div>

              <div class="left">
                <div class="">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Pembayaran</li>
                    </ol>
                </div>
              </div>

            </div>

            <div class="clearfix"></div>

            <!-- message -->
            <div class="alert alert-warning" role="alert">
              <strong>Catatan</strong> 
              <ul>
                <li>Foto KTP tidak sesuai</li>
                <li>Transkrip nilai tidak sesuai</li>
              </ul>
            </div>

            <div class="form_box">

                <form action="#" class="form-horizontal form-label-left">

                  <div class="form-group border">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">KTP
                    </label>
                    <div class="input-file col-md-4 col-sm-4 col-xs-12">
                      <input type="file" name="" id="img1"><label>Browse</label>
                      <img data-toggle="modal" data-target=".preview-img" id="preview-img1" />
                    </div>
                  </div>

                  <div class="form-group border">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Ijazah
                    </label>
                    <div class="input-file col-md-4 col-sm-4 col-xs-12">
                      <input type="file" name="" id="img2"><label>Browse</label>
                      <img data-toggle="modal" data-target=".preview-img" id="preview-img2" />
                    </div>
                  </div>

                  <div class="form-group border">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Transkrip Nilai
                    </label>
                    <div class="input-file col-md-4 col-sm-4 col-xs-12">
                      <input type="file" name="" id="img3"><label>Browse</label>
                      <img data-toggle="modal" data-target=".preview-img" id="preview-img3" />
                    </div>
                  </div>

                  <div class="form-group border">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Sertifikat Kader
                    </label>
                    <div class="input-file col-md-4 col-sm-4 col-xs-12">
                      <input type="file" name="" id="img4"><label>Browse</label>
                      <img data-toggle="modal" data-target=".preview-img" id="preview-img4" />
                    </div>
                  </div>

                  <div class="form-group border">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Sertifikat Prestasi
                    </label>
                    <div class="input-file col-md-4 col-sm-4 col-xs-12">
                      <input type="file" name="" id="img5"><label>Browse</label>
                      <img data-toggle="modal" data-target=".preview-img" id="preview-img5" />
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                      <button type="button" class="btn btn-success" data-toggle="modal" data-target=".submit-form">Simpan</button>
                      <button type="button" class="btn btn-default">Batal</button>
                    </div>
                  </div>

                  <!-- modal -->
                  <div class="modal fade preview-img" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">File Berkas</h4>
                        </div>
                        <div class="modal-body">
                          <img class="preview-gbr" src="">
                        </div>
                        <div class="modal-footer center ">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>

                </div>
              </div>
            </div>

                  <!-- modal -->
                  <div class="modal fade submit-form" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2"></h4>
                        </div>
                        <div class="modal-body">
                          Data yang disubmit akan tersimpan dan tidak dapat diubah
                        </div>
                        <div class="modal-footer center ">
                          <button type="submit" class="btn btn-primary">Submit</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        </div>

                </div>
              </div>
            </div>

                </form>

            </div>

          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<script type="text/javascript">
  function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                imgId = '#preview-'+$(input).attr('id');
                $(imgId).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
          var imgIds = '#preview-'+$(input).attr('id');
          $(imgIds).on("click",function(){
            var Imgs=$(this).attr('src');
            $('.preview-gbr').attr('src',Imgs);
          });
      }


      $(".input-file input[type='file']").change(function(){
        readURL(this);
      });
</script>