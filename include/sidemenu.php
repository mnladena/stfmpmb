<!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Menu</h3>
                <ul class="nav side-menu">
                  <li><a href="index.php"><i class="fa fa-home"></i> Halaman Utama</a></li>

                  <li><a href="pendaftaran.php"><i class="fa fa-book"></i> Formulir Pendaftaran</a></li>

                  <li><a href="pembayaran.php"><i class="fa fa-money"></i> Pembayaran</a></li>

                  <li><a href="ujian.php"><i class="fa fa-pencil"></i> Ujian Masuk</a></li>

                  

                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
            </nav>
            <div class="head-title">PMB STFM</div>
          </div>
        </div>
        <!-- /top navigation -->