<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Halaman Utama</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="step">
              <div class="step-item selesai"><i class="fa fa-check"></i><span>START</span></div>
              <div class="step-item selesai"><i class="fa fa-check"></i><span>Pembayaran Registrasi</span></div>
              <div class="step-item"><i class="fa fa-check"></i><span>Form Registrasi</span></div>
              <div class="step-item"><i class="fa fa-check"></i><span>Ujian Masuk</span></div>
              <div class="step-item"><i class="fa fa-check"></i><span>Pembayaran Kuliah</span></div>
              <div class="step-item"><i class="fa fa-check"></i><span>Kelengkapan Berkas</span></div>
              <div class="step-item"><i class="fa fa-check"></i><span>Finish</span></div>
            </div>

            <div class="row">

              <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Informasi Pendaftaran 2019/2020</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <ul class="pengumuman-dashboard">
                        <li>
                          <div class="date-time">22/09/2019 <br> 12:00</div>
                          <div class="info-desc">
                            <span class="lbl">Pembayaran</span>
                            Pembayaran pendaftaran sudah divalidasi. Terima kasih telah melakukan pembayaran.
                          </div>
                        </li>
                        <li>
                          <div class="date-time">22/09/2019 <br> 13:00</div>
                          <div class="info-desc">
                            <span class="lbl">Pembayaran</span>
                            Pembayaran pendaftaran sukses dikirim.
                          </div>
                        </li>
                        <li>
                          <div class="date-time">22/09/2019 <br> 12:00</div>
                          <div class="info-desc">
                            <span class="lbl">Pembayaran</span>
                            Pembayaran pendaftaran sudah divalidasi. Terima kasih telah melakukan pembayaran.
                          </div>
                        </li>
                        <li>
                          <div class="date-time">22/09/2019 <br> 13:00</div>
                          <div class="info-desc">
                            <span class="lbl">Pembayaran</span>
                            Pembayaran pendaftaran sukses dikirim.
                          </div>
                        </li>
                        <li>
                          <div class="date-time">22/09/2019 <br> 12:00</div>
                          <div class="info-desc">
                            <span class="lbl">Pembayaran</span>
                            Pembayaran pendaftaran sudah divalidasi. Terima kasih telah melakukan pembayaran.
                          </div>
                        </li>
                        <li>
                          <div class="date-time">22/09/2019 <br> 13:00</div>
                          <div class="info-desc">
                            <span class="lbl">Pembayaran</span>
                            Pembayaran pendaftaran sukses dikirim.
                          </div>
                        </li>
                        <li>
                          <div class="date-time">22/09/2019 <br> 12:00</div>
                          <div class="info-desc">
                            <span class="lbl">Pembayaran</span>
                            Pembayaran pendaftaran sudah divalidasi. Terima kasih telah melakukan pembayaran.
                          </div>
                        </li>
                        <li>
                          <div class="date-time">22/09/2019 <br> 13:00</div>
                          <div class="info-desc">
                            <span class="lbl">Pembayaran</span>
                            Pembayaran pendaftaran sukses dikirim.
                          </div>
                        </li>
                        <li>
                          <div class="date-time">22/09/2019 <br> 12:00</div>
                          <div class="info-desc">
                            <span class="lbl">Pembayaran</span>
                            Pembayaran pendaftaran sudah divalidasi. Terima kasih telah melakukan pembayaran.
                          </div>
                        </li>
                        <li>
                          <div class="date-time">22/09/2019 <br> 13:00</div>
                          <div class="info-desc">
                            <span class="lbl">Pembayaran</span>
                            Pembayaran pendaftaran sukses dikirim.
                          </div>
                        </li>
                      </ul>
                      <nav aria-label="Page navigation example">
                        <ul class="pagination">
                          <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1">Previous</a>
                          </li>
                          <li class="page-item"><a class="page-link" href="#">1</a></li>
                          <li class="page-item"><a class="page-link" href="#">2</a></li>
                          <li class="page-item"><a class="page-link" href="#">3</a></li>
                          <li class="page-item">
                            <a class="page-link" href="#">Next</a>
                          </li>
                        </ul>
                      </nav>
                  </div>
                </div>
              </div>

              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Informasi Ujian PMB</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table table-striped">
                      <tr>
                        <td>Tanggal</td>
                        <td width="30" align="center">:</td>
                        <td>11 September 2019</td>
                      </tr>
                      <tr>
                        <td>Waktu</td>
                        <td width="30" align="center">:</td>
                        <td>11.00 - 14.00</td>
                      </tr>
                      <tr>
                        <td>Tempat</td>
                        <td width="30" align="center">:</td>
                        <td>Ruang CBT STFM</td>
                      </tr>
                      <tr>
                        <td>Hasil Ujian</td>
                        <td width="30" align="center">:</td>
                        <td>Lulus</td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>


            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>
