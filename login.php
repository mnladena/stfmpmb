<?php include "include/head.php" ?>

  <script type="text/javascript">
      var onloadCallback = function() {
        grecaptcha.render('html_element', {
          'sitekey' : 'your_site_key'
        });
      };
    </script>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <!-- <div class="left-main">
          <div class="left-main-content">
            <div class="big-title-home"><span>Telah dibuka!</span> Pendaftaran calon mahasiswa baru</div>
            <div class="med-title-home">Tahun 2019 - Gelombang 4</div>
            <div class="med-title-home">Mulai tanggal 15 Mei 2019 - 15 Juni 2019</div>
            <div class="med-title-warning">Jangan sampai terlewat!</div>
            <div class="btn-wrap"><a class="btn btn-success" href="daftar.php">Klik di sini untuk mendaftar</a></div>
          </div>
      </div> -->

      <div class="login_wrapper">
        <div class="form login_form">
          <div class="site_logo center"><img src="images/logo.png" alt=""></div>
          <div class="center big_title">Calon Mahasiswa Baru</div>
          <div class="center med_title">Tahun 2019 - Gelombang 3</div>
          <section class="login_content">
            <form>
              <h1>Login</h1>
              <div>
                <input type="text" class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div id="html_element"></div>
              <div>
                <a class="btn btn-success submit" href="index.php">Log in</a>
                <a class="reset_pass" href="#">Lupa password?</a>
              </div>

              <div class="clearfix"></div>

            </form>
            <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer>
            </script>
          </section>
        </div>

      </div>
    </div>
  </body>
</html>
