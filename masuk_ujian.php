<?php include "include/head.php" ?>

  <body>
    <div class="container body">
      <div class="main_container">
        
        <!-- header -->
        <div class="header-soal col-md-12 col-sm-12">

          <div class="pull-left profil-siswa">
            <div class="img-siswa imgLiquidFill">
              <img src="images/user.jpg">
            </div>
            <span class="nama-siswa">Rini Handayani (034500001)</span>
          </div>

          <button class="btn-stop pull-right btn btn-primary"><i class="fa fa-stop"></i>  Selesai Ujian</button>

        </div>
        <!-- header -->

        <div class="clearfix"></div>
        <div class="page-content">
          <div class="wp-page">
            <div class="big-title">Halo, Rini Handayani</div>
            <div class="desc-title">Jika kamu sudah siap, kamu bisa tekan tombol mulai di bawah ini. Setelah kamu memulai ujian, kamu tidak bisa kembali sampai menyelesaikan semua soal ujian.</div>
            <a class="btn btn-big btn-success" href="soal_ujian.php">Mulai</a>
          </div>
        </div>

<?php include "include/footer.php" ?>


