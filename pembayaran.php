<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_center">
                <h3>Pembayaran</h3>
              </div>

              <div class="left">
                <div class="">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Pembayaran</li>
                    </ol>
                </div>
              </div>

            </div>

            <div class="clearfix"></div>

            <!-- message -->
            <div class="alert alert-success alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <strong>Sukses!</strong> Submit pembayaran berhasil, silahkan tunggu pengecekan
            </div>

            <div class="form_box">

              <div class="box-daftar aktif">
                <div class="box-lock"><span><i class="fa fa-lock"></i></span></div>

              <?php if(isset($_GET["cek"])==1)
              {
                echo '<div class="box-daftar-title success">Berhasil Dicek</div>';
              }else{
                echo '<div class="box-daftar-title check">Proses Pengecekan</div>';
              }?>
              <div class="box-daftar-desc">
                  <i class="fa fa-money"></i>
                  <div class="box-daftar-name">Bayar Pendaftaran</div>
                </div> 
                <div class="box-daftar-btn">
                  <a class="btn btn-primary" href="form_bayar.php">Bayar</a>
                  <a class="btn btn-info" href="berkas_bayar.php">Berkas</a>
                </div>
              </div>


              <?php if(isset($_GET['lulus'])==1)
              {
                  
              echo' 
              <div class="box-daftar aktif">
                <div class="box-lock"><span><i class="fa fa-lock"></i></span></div>

                <div class="box-daftar-title non-aktif">Belum Upload</div>

                <div class="box-daftar-desc">
                  <i class="fa fa-archive"></i>
                  <div class="box-daftar-name">Bayar Perkuliahan</div>
                </div>
                <div class="box-daftar-btn">
                  <a class="btn btn-primary" href="form_bayar_kuliah.php">Bayar</a>
                  <a class="btn btn-info" href="berkas_bayar_kuliah.php">Berkas</a>
                </div>
              </div>  
                  ';
              }
              else{
                echo'
              <div class="box-daftar">
                <div class="box-lock"><span><i class="fa fa-lock"></i></span></div>

                <div class="box-daftar-title non-aktif">Belum Aktif</div>

                <div class="box-daftar-desc">
                  <i class="fa fa-archive"></i>
                  <div class="box-daftar-name">Bayar Perkuliahan</div>
                </div>
                <div class="box-daftar-btn">
                  <a class="btn btn-primary" href="form_bayar_kuliah.php">Bayar Kuliah</a>
                  <a class="btn btn-info" href="berkas_bayar_kuliah.php">Berkas</a>
                </div>
              </div>  
                  ';
              }
              ?>

              

            </div>

            <div class="clearfix"></div>
              <div class="alert alert-warning text-left mt15" role="info">
                <strong><i class="fa fa-info-circle"></i></strong> NOTES:
                <ul>
                  <li>Tombol formulir dan berkas akan aktif ketika telah melakukan pembayaran</li>
                  <li>Formulir dan berkas yang sudah disimpan akan diverifikasi terlebih dahulu</li>
                  <li>Jika formulir dan berkas ditemukan tidak valid, maka calon mahasiswa tidak akan bisa melanjutkan proses ujian masuk</li>
                </ul>
              </div>

          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>
