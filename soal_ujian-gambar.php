<?php include "include/head.php" ?>

  <body>
    <div class="container body">
      <div class="main_container">
        
        <!-- header -->
        <div class="header-soal col-md-12 col-sm-12">

          <div class="pull-left profil-siswa">
            <div class="img-siswa imgLiquidFill">
              <img src="images/user.jpg">
            </div>
            <span class="nama-siswa">Rini Handayani (034500001)</span>
          </div>

          <button class="btn-stop pull-right btn btn-primary"><i class="fa fa-stop"></i>  Selesai Ujian</button>

        </div>
        <!-- header -->

        <div class="page-content">

          <!-- left-menu -->
          <div class="left-menu col-md-3 col-sm-12 col-xs-12">

            <div class="timer">
              01:30:20
            </div>

            <div class="soal-ujian">

              <!-- start accordion -->
              <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">

                <div class="panel">
                  <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#soal_bhsindonesia" aria-expanded="true" aria-controls="collapseOne">
                    <h4 class="panel-title">Bahasa Indonesia (5/25)</h4>
                  </a>
                  <div id="soal_bhsindonesia" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <table class="tabel-soal">
                          <tr>
                          <?php
                          for ($i=1; $i < 6; $i++) { 
                            echo'
                              <td><a href="#">'.$i.':?</a></td>
                            ';
                          }
                          ?>
                          </tr>
                          <tr>
                          <?php
                          for ($j=6; $j < 11; $j++) { 
                            echo'
                              <td><a href="#">'.$j.':?</a></td>
                            ';
                          }
                          ?>
                          </tr>
                          <tr>
                          <?php
                          for ($k=11; $k < 16; $k++) { 
                            echo'
                              <td><a href="#">'.$k  .':?</a></td>
                            ';
                          }
                          ?>
                          </tr>
                          <tr>
                          <?php
                          for ($l=16; $l < 21; $l++) { 
                            echo'
                              <td><a href="#">'.$l.':?</a></td>
                            ';
                          }
                          ?>
                          </tr>
                          <tr>
                          <?php
                          for ($m=21; $m < 26; $m++) { 
                            echo'
                              <td>'.$m.':?</td>
                            ';
                          }
                          ?>
                          </tr>
                        </table>
                    </div>
                  </div>
                </div>

                <div class="panel">
                  <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#soal_bhsinggris" aria-expanded="false" aria-controls="collapseTwo">
                    <h4 class="panel-title">Bahasa Inggris (5/25)</h4>
                  </a>
                  <div id="soal_bhsinggris" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                      <table class="tabel-soal">
                          <tr>
                          <?php
                          for ($i=1; $i < 6; $i++) { 
                            echo'
                              <td><a href="#">'.$i.':?</a></td>
                            ';
                          }
                          ?>
                          </tr>
                          <tr>
                          <?php
                          for ($j=6; $j < 11; $j++) { 
                            echo'
                              <td><a href="#">'.$j.':?</a></td>
                            ';
                          }
                          ?>
                          </tr>
                          <tr>
                          <?php
                          for ($k=11; $k < 16; $k++) { 
                            echo'
                              <td><a href="#">'.$k  .':?</a></td>
                            ';
                          }
                          ?>
                          </tr>
                          <tr>
                          <?php
                          for ($l=16; $l < 21; $l++) { 
                            echo'
                              <td><a href="#">'.$l.':?</a></td>
                            ';
                          }
                          ?>
                          </tr>
                          <tr>
                          <?php
                          for ($m=21; $m < 26; $m++) { 
                            echo'
                              <td>'.$m.':?</td>
                            ';
                          }
                          ?>
                          </tr>
                        </table>
                    </div>
                  </div>
                </div>

                <div class="panel">
                  <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#soal_matematika" aria-expanded="false" aria-controls="collapseThree">
                    <h4 class="panel-title">Matematika (5/25)</h4>
                  </a>
                  <div id="soal_matematika" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                      <table class="tabel-soal">
                          <tr>
                          <?php
                          for ($i=1; $i < 6; $i++) { 
                            echo'
                              <td><a href="#">'.$i.':?</a></td>
                            ';
                          }
                          ?>
                          </tr>
                          <tr>
                          <?php
                          for ($j=6; $j < 11; $j++) { 
                            echo'
                              <td><a href="#">'.$j.':?</a></td>
                            ';
                          }
                          ?>
                          </tr>
                          <tr>
                          <?php
                          for ($k=11; $k < 16; $k++) { 
                            echo'
                              <td><a href="#">'.$k  .':?</a></td>
                            ';
                          }
                          ?>
                          </tr>
                          <tr>
                          <?php
                          for ($l=16; $l < 21; $l++) { 
                            echo'
                              <td><a href="#">'.$l.':?</a></td>
                            ';
                          }
                          ?>
                          </tr>
                          <tr>
                          <?php
                          for ($m=21; $m < 26; $m++) { 
                            echo'
                              <td>'.$m.':?</td>
                            ';
                          }
                          ?>
                          </tr>
                        </table>
                    </div>
                  </div>
                </div>

                <div class="panel">
                  <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#soal_gambar" aria-expanded="false" aria-controls="collapseThree">
                    <h4 class="panel-title">Gambar (5/25)</h4>
                  </a>
                  <div id="soal_gambar" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                      <table class="tabel-soal">
                          <tr>
                          <?php
                          for ($i=1; $i < 6; $i++) { 
                            echo'
                              <td><a href="#">'.$i.':?</a></td>
                            ';
                          }
                          ?>
                          </tr>
                          <tr>
                          <?php
                          for ($j=6; $j < 11; $j++) { 
                            echo'
                              <td><a href="#">'.$j.':?</a></td>
                            ';
                          }
                          ?>
                          </tr>
                          <tr>
                          <?php
                          for ($k=11; $k < 16; $k++) { 
                            echo'
                              <td><a href="#">'.$k  .':?</a></td>
                            ';
                          }
                          ?>
                          </tr>
                          <tr>
                          <?php
                          for ($l=16; $l < 21; $l++) { 
                            echo'
                              <td><a href="#">'.$l.':?</a></td>
                            ';
                          }
                          ?>
                          </tr>
                          <tr>
                          <?php
                          for ($m=21; $m < 26; $m++) { 
                            echo'
                              <td>'.$m.':?</td>
                            ';
                          }
                          ?>
                          </tr>
                        </table>
                    </div>
                  </div>
                </div>


              </div>
              <!-- end of accordion -->
              
            </div>


          </div>
          <!-- left-menu -->

          <!-- right-menu -->
          <div class="right-menu col-md-9">

            <div class="alert alert-info center">
                <h4>Isilah soal ujian dengan baik dan benar, telitilah dalam menjawab soal</h4>
            </div>

            <!-- soal ujian -->
            <div class="soal">
              <span class="nomor-soal">1</span>
              <div class="soal-text">
                 Bentuk paruh burung yang panjang dan tebal menunjukkan adanya adaptasi secara:
              </div>
            </div>

            <ul class="list-jawaban_ujian">
              <li>
                <div class="radio pilihan-jawaban">
                    <span class="abjad">A</span>
                    <input required="required" type="radio" class="flat left" value="" id="pilihan1" name="pilihan"> 
                    <label for="pilihan1" class="inp-text left">
                      Fisiologi
                      <img src="images/elang.jpg">
                    </label>
                </div>
              </li>
              <li>
                <div class="radio pilihan-jawaban">
                  <span class="abjad">B</span>
                    <input required="required" type="radio" class="flat left" value="jenjang1" id="pilihan2" name="pilihan"> 
                    <label for="pilihan2" class="inp-text left">
                    Fisiologi
                    <img src="images/elang.jpg">
                  </label>
                </div>
              </li>
              <li>
                <div class="radio pilihan-jawaban">
                  <span class="abjad">C</span>
                    <input required="required" type="radio" class="flat left" value="jenjang1" id="pilihan3" name="pilihan"> 
                    <label for="pilihan3" class="inp-text left">
                      Morfologi
                      <img src="images/elang.jpg">
                    </label>
                </div>
              </li>
              <li>
                <div class="radio pilihan-jawaban">
                  <span class="abjad">D</span>
                    <input required="required" type="radio" class="flat left" value="jenjang1" id="pilihan4" name="pilihan"> 
                    <label for="pilihan4" class="inp-text left">
                      Biologi
                      <img src="images/elang.jpg">
                    </label>
                </div>
              </li>
              <li>
                <div class="radio pilihan-jawaban">
                  <span class="abjad">E</span>
                    <input required="required" type="radio" class="flat left" value="jenjang1" id="pilihan5" name="pilihan"> 
                    <label for="pilihan5" class="inp-text left">
                      Tingkah laku
                      <img src="images/elang.jpg">
                    </label>
                </div>
              </li>
            </ul>

            <div class="soal-nav">
              <button class="btn btn-primary"><i class="fa fa-arrow-left"></i> Pertanyaan Sebelumnya</button>
              <button class="btn btn-primary">Pertanyaan Selanjutnya <i class="fa fa-arrow-right"></i></button>
              <button class="btn btn-success"><i class="fa fa-flag-checkered"></i> Akhiri Ujian</button>
            </div>
            
          </div>
          <!-- right-menu -->


        </div>

<?php include "include/footer.php" ?>


