<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_center">
                <h3>Ujian Masuk</h3>
              </div>

              <div class="left">
                <div class="">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Ujian</li>
                    </ol>
                </div>
              </div>

            </div>

            <div class="clearfix"></div>

            <?php if(isset($_GET['lulus'])==1)
              {
                echo '
            <div class="alert alert-success center" role="alert">
              <strong>Selamat!</strong> kamu lulus ujian PMB!
            </div>';
            }
            else{}
            ?>

            <div class="form_box">

              <?php if(isset($_GET['lulus'])==1)
              {
                  
              echo' 
              <div class="box-daftar aktif">
                <div class="box-lock"><span><i class="fa fa-lock"></i></span></div>

                <div class="box-daftar-title">Tersedia</div>
                <a href="masuk_ujian.php">
                  <div class="box-daftar-desc">
                    <i class="fa fa-graduation-cap"></i>
                    <div class="box-daftar-name">Ujian Masuk</div>
                  </div>
                </a>
              </div>  
                  ';
              }
              else{
                echo'
              <div class="box-daftar">
                <div class="box-lock"><span><i class="fa fa-lock"></i></span></div>

                <a href="masuk_ujian.php">
                  <div class="box-daftar-desc">
                    <i class="fa fa-graduation-cap"></i>
                    <div class="box-daftar-name">Ujian Masuk</div>
                  </div>
                </a>

              </div>  
                  ';
              }
              ?>

              

            </div>

            <div class="clearfix"></div>
              <div class="alert alert-warning text-left mt15" role="info">
                <strong><i class="fa fa-info-circle"></i></strong> NOTES:
                <ul>
                  <li>Ujian masuk akan dibuka saat sudah masuk tanggal ujian</li>
                  <li>Ujian masuk bisa dilakukan saat calon mahasiswa sudah diabsen oleh panitia saat kehadiran ujian</li>
                </ul>
              </div>

          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>
