<?php include "include/head.php" ?>

  <script type="text/javascript">
      var onloadCallback = function() {
        grecaptcha.render('html_element', {
          'sitekey' : 'your_site_key'
        });
      };
    </script>

  <body class="login">
    <div>

      <div class="left-main">
          <div class="left-main-content">
             <div class="site_logo center"><img src="images/logo.png" alt="" width="100"></div>

             <div class="welcome-wrap">
                <div class="big-title-home"><span>Telah dibuka!</span> Pendaftaran calon mahasiswa baru <br> Tahun 2019 - Gelombang 4</div>

              
                <div class="med-title-home"></div>
                <div class="med-title-home"><strong>- Mulai tanggal 15 Mei 2019 - 15 Juni 2019 -</strong></div>

                <div class="med-title-warning">Jangan sampai terlewat!</div>

                <div class="btn-wrap"><a class="btn btn-login" href="login.php">Login di sini</a></div>
                <div class="mt15">Belum punya akun? silahkan <a class="btn-text" href="daftar.php">daftar di sini</a></div>
            </div>

          </div>
      </div>

    </div>
  </body>
</html>
